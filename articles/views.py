from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from rest_framework import status, viewsets
from rest_framework import generics
import json
from django.views.generic.base import View
from django.http import JsonResponse
from constance import config
from django.conf import settings
from rest_framework import permissions
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from .serializers import ArticleSerializer, SectionSerializer, RegardSerializer, ReviewSerializer
from .models import Article, Section, Regard, Review, Tag, Client
import telegram

import logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
            level=logging.INFO)

# from django.contrib.auth.models import User
class ArticleListView(APIView):
    # permission_classes = (permissions.IsAdminUser,)
    def get(self, request, **kwargs):
        if kwargs.get('count'):
            articles = Article.objects.all().order_by('-public_date')[:int(kwargs.get('count'))]
        else:
            articles = Article.objects.all().order_by('-public_date')
        parsed_articles = ArticleSerializer(articles, context={'request': request}, many=True)
        return Response(parsed_articles.data)

class ArticleTagView(APIView):
    # permission_classes = (permissions.IsAdminUser,)
    def get(self, request, **kwargs):
        if kwargs.get('tag') is not None:
            tag = Tag.objects.get(name=kwargs.get('tag'))
            articles = Article.objects.filter(tags__in=[tag]).order_by('-public_date')
            parsed_articles = ArticleSerializer(articles, context={'request': request}, many=True)
        else:
            articles = Article.objects.all().order_by('-public_date')
            parsed_articles = ArticleSerializer(articles, context={'request': request}, many=True)
        return Response(parsed_articles.data)

class ArticleView(APIView):
    # permission_classes = (permissions.IsAdminUser,)
    def get(self, request, **kwargs):
        if kwargs.get('slug') is not None:
            article = Article.objects.get(slug=kwargs.get('slug'))
            parsed_articles = ArticleSerializer(article, context={'request': request})
        else:
            articles = Article.objects.all().order_by('-public_date')
            parsed_articles = ArticleSerializer(articles, context={'request': request}, many=True)
        return Response(parsed_articles.data)

class RegardListView(generics.ListCreateAPIView):
    # permission_classes = (permissions.IsAdminUser,)
    queryset = Regard.objects.all()
    serializer_class = RegardSerializer

class ReviewListView(generics.ListCreateAPIView):
    # permission_classes = (permissions.IsAdminUser,)
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer

class ArticleDetailView(generics.RetrieveUpdateDestroyAPIView):
    # permission_classes = (permissions.IsAdminUser,)
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer

class SectionView(viewsets.ViewSet):
    # permission_classes = (permissions.IsAdminUser,)
    def list(self, request):
        queryset = Section.objects.all()
        serializer = SectionSerializer(queryset, many=True)
        return Response(serializer.data)

class OneSectionView(APIView):
    # permission_classes = (permissions.IsAdminUser,)
    def get(self, request, **kwargs):
        sec_id = kwargs['pk']
        print(sec_id)
        section = Section.objects.get(pk=int(sec_id))
        articles = Article.objects.filter(sections=section).order_by('-public_date')
        parsed_section = SectionSerializer(section)
        parsed_articles = ArticleSerializer(articles, context={'request': request}, many=True)
        return Response({"section": parsed_section.data, "articles": parsed_articles.data})

class ConfigView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ConfigView, self).dispatch(request, *args, **kwargs)
    def get(self, request):
        quote = config.MAIN_BLOCKQUOTE
        desc = config.MAIN_DESCRIPTION
        return JsonResponse({"blockquote": quote, "description": desc})



class QuestionView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(QuestionView, self).dispatch(request, *args, **kwargs)

    def post(self, request):
        data = json.loads(request.body.decode('utf-8'))
        # name = data.get('name')
        phone = data.get('phone')
        # wantchat = data.get('wantchat')
        # city = data.get('city')
        # question = data.get('question')
        # service = data.get('service')
        # chat = ''
        # if wantchat == 'want':
        #     chat = 'телеграм установлен, хочет ответ в чат'
        # else:
        #     chat = 'телеграм не установлен/не хочет ответ в чат'
        # if not service:
        #     service = 'главная страница'
        # message = """
        # Сэр, новый клиент с сайта okrupkin.com!
        # Вам написал(а) %s, из %s, телефон %s,
        # %s, из раздела %s,
        # вопрос: %s
        # """
        message = """
        Сэр, новый клиент с сайта okrupkin.com!
        Телефон: %s
        """
        Client.objects.create(
            # name=name,
            phone=phone,
            # city=city,
            # question=question,
            # service=service
        )
        try:
            bot = telegram.Bot(settings.TELEGRAM_TOKEN)
            # bot.send_message(353759932, message % (name, city, phone, chat, service, question))
            bot.send_message(353759932, message % phone)
        except:
            pass
        return JsonResponse({"success": "ok"})
