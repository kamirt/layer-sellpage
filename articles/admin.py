from django.contrib import admin
from django import forms
from ckeditor.widgets import CKEditorWidget
from .models import Article, Section, MetaData, Tag, Regard, Review, Client
# Register your models here.


class ArticleAdminForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())
    class Meta:
        model = Article
        fields = '__all__'

class ServiceAdminForm(forms.ModelForm):
    description = forms.CharField(widget=CKEditorWidget())
    class Meta:
        model = Section
        fields = '__all__'

class ArticleAdmin(admin.ModelAdmin):
    form = ArticleAdminForm
admin.site.register(Article, ArticleAdmin)

class SectionAdmin(admin.ModelAdmin):
    form = ServiceAdminForm
admin.site.register(Section, SectionAdmin)

class MetaAdmin(admin.ModelAdmin):
    pass
admin.site.register(MetaData, MetaAdmin)


class TagAdmin(admin.ModelAdmin):
    pass
admin.site.register(Tag, TagAdmin)

class RegardAdmin(admin.ModelAdmin):
    pass
admin.site.register(Regard, RegardAdmin)


class ReviewAdmin(admin.ModelAdmin):
    pass
admin.site.register(Review, ReviewAdmin)

class ClientAdmin(admin.ModelAdmin):
    pass
admin.site.register(Client, ClientAdmin)
