from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    url(r'^$', views.ArticleListView.as_view()),
    url(r'^question/$', views.QuestionView.as_view()),
    url(r'^sections/$', views.SectionView.as_view({'get': 'list'})),
    url(r'^tag/(?P<tag>[а-я А-Я]+|[a-z A-Z]+)/$', views.ArticleTagView.as_view()),
    url(r'^regards/$', views.RegardListView.as_view()),
    url(r'^reviews/$', views.ReviewListView.as_view()),
    url(r'^config/$', views.ConfigView.as_view()),
    url(r'^sections/(?P<pk>[0-9]+)/$', views.OneSectionView.as_view()),
    url(r'^article/(?P<pk>[0-9]+)/$', views.ArticleDetailView.as_view()),
    url(r'^(?P<slug>([a-z A-Z]+[-]?)+)/$', views.ArticleView.as_view()),
    url(r'^(?P<count>[0-9]+)/$', views.ArticleListView.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
