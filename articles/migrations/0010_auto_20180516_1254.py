# Generated by Django 2.0.2 on 2018-05-16 12:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0009_auto_20180515_1317'),
    ]

    operations = [
        migrations.RenameField(
            model_name='article',
            old_name='html_content',
            new_name='content',
        ),
    ]
