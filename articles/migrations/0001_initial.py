# Generated by Django 2.0.2 on 2018-02-15 18:09

import autoslug.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_added', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Дата добавления')),
                ('date_updated', models.DateTimeField(auto_now=True, db_index=True, verbose_name='Дата обновления')),
                ('order', models.PositiveIntegerField(default=0, verbose_name='Порядок')),
                ('headline', models.CharField(max_length=400, verbose_name='Заголовок')),
                ('html_content', models.TextField(verbose_name='Содержание статьи')),
                ('url', models.URLField(verbose_name='Ссылка на статью')),
                ('slug', autoslug.fields.AutoSlugField(editable=False, populate_from='headline', unique=True, verbose_name='Транслитерация')),
                ('public_date', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Дата публикации')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Section',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order', models.PositiveIntegerField(default=0, verbose_name='Порядок')),
                ('name', models.CharField(max_length=200)),
            ],
            options={
                'ordering': ['order'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('articles', models.ManyToManyField(to='articles.Article', verbose_name='Статьи')),
            ],
        ),
        migrations.AddField(
            model_name='article',
            name='sections',
            field=models.ManyToManyField(to='articles.Section', verbose_name='Разделы'),
        ),
    ]
