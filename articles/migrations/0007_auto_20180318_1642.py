# Generated by Django 2.0.2 on 2018-03-18 16:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('articles', '0006_auto_20180318_1610'),
    ]

    operations = [
        migrations.AddField(
            model_name='section',
            name='description',
            field=models.TextField(default=' ', verbose_name='Описание услуги/раздела'),
        ),
        migrations.AddField(
            model_name='section',
            name='short_description',
            field=models.CharField(blank=True, max_length=300, verbose_name='Краткое описание'),
        ),
        migrations.AddField(
            model_name='subsection',
            name='description',
            field=models.TextField(default=' ', verbose_name='Описание услуги/раздела'),
        ),
        migrations.AddField(
            model_name='subsection',
            name='short_description',
            field=models.CharField(blank=True, max_length=300, verbose_name='Краткое описание'),
        ),
    ]
