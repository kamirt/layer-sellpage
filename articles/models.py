# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse

from autoslug.fields import AutoSlugField

from core.models import TimeStampedModel, OrderedModel


class MetaData(models.Model):
    title = models.CharField(_('Тег title'), max_length=250)
    meta_title = models.CharField(_('Meta title'), max_length=250)
    meta_description = models.TextField(_('Meta description'))

    def __str__(self):
        return self.title

    class Meta():
        verbose_name='мета-данные'
        verbose_name_plural='Мета-данные'


class Section(OrderedModel):
    name = models.CharField(max_length=200)
    description = models.TextField(_('Описание услуги/раздела'), default=' ')
    short_description = models.CharField(_('Краткое описание'), max_length=300, blank=True)
    icon = models.ImageField(_('Иконка'), upload_to="img/icons", null=True)
    meta_info = models.ForeignKey(MetaData, verbose_name=_('Метатеги'), blank=True, on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.name

    class Meta():
        verbose_name='раздел/услуга'
        verbose_name_plural='Разделы/услуги'


class Tag(models.Model):
    name = models.CharField(max_length=200)
    def __str__(self):
        return self.name
    class Meta():
        verbose_name='Тег'
        verbose_name_plural='Теги'


class Article(TimeStampedModel, OrderedModel):
    headline = models.CharField(_('Заголовок'), max_length=400)
    content = models.TextField(_('Содержание статьи'))
    url = models.URLField(_('Ссылка на статью'), blank=True)
    slug = AutoSlugField(populate_from='headline',
     unique=True,
     verbose_name='Транслитерация',
     db_index=True)
    sections = models.ForeignKey(Section, verbose_name='Раздел', on_delete=models.SET_NULL, null=True, blank=True)
    public_date = models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Дата публикации')
    meta_data = models.ForeignKey(MetaData, verbose_name='Мета данные', on_delete=models.CASCADE, null=True)
    image = models.ImageField(_("Изображение"), upload_to='img', null=True)
    tags = models.ManyToManyField(Tag, verbose_name=_('Теги'))

    def __str__(self):
        return self.headline

    def get_absolute_url(self):
        return reverse('articles.views.ArticleListView', args=[str(self.slug)])

    class Meta():
        verbose_name='статью'
        verbose_name_plural='Статьи'


class Regard(models.Model):
    image = models.ImageField(_("Изображение"), upload_to='img/regards', null=True)
    name = models.CharField(_('Название'), max_length=300)
    description = models.TextField(_('Описание'))
    date = models.DateField(_('Дата вручения'))

    class Meta():
        verbose_name='Награда'
        verbose_name_plural='Награды'


class Review(models.Model):
    avatar = models.ImageField(_("Фото"), upload_to='img/reviews', blank=True, null=True)
    text = models.TextField(_('Текст'))
    name = models.CharField(_('ФИО'), max_length=400)
    class Meta():
        verbose_name='Отзыв'
        verbose_name_plural='Отзывы'


class Client(models.Model):
    name = models.CharField(_('Имя'), max_length=400, blank=True, null=True)
    phone = models.CharField(_('Телефон'), max_length=20, blank=True, null=True)
    city = models.CharField(_('Город'), max_length=400, blank=True, null=True)
    question = models.TextField(_('Вопрос'), null=True, blank=True)
    service = models.CharField(_("Услуга"), max_length=400, blank=True, null=True)

    def __str__(self):
        return self.name + ' ' + self.city

    class Meta():
        verbose_name='клиент'
        verbose_name_plural='Клиенты'
