from rest_framework import serializers
from .models import Article, Section, MetaData, Regard, Review

class ArticleSerializer(serializers.ModelSerializer):
    tags = serializers.StringRelatedField(many=True)
    sections = serializers.StringRelatedField()
    class Meta:
        model = Article
        fields = '__all__'

class SectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Section
        fields = ('id', 'name', 'short_description', 'description', 'icon')

class MetaDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = MetaData

class RegardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Regard
        fields = '__all__'

class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = '__all__'
