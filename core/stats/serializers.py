from rest_framework import serializers
from .models import EventWatcher

class EventWatcherSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventWatcher
        fields = ('id', 'event', 'element', 'page', 'ip', 'lat', 'lng', 'location')
