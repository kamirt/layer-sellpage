from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from rest_framework import status
from rest_framework import generics

from .serializers import EventWatcherSerializer
from .models import EventWatcher
# from django.contrib.auth.models import User
class EventListView(generics.ListCreateAPIView):

    queryset = EventWatcher.objects.all()
    serializer_class = EventWatcherSerializer

class EventDetailView(generics.RetrieveUpdateDestroyAPIView):
    queryset = EventWatcher.objects.all()
    serializer_class = EventWatcherSerializer
