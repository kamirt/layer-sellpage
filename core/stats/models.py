# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse
from lizart_profile.models import LizartUser
from core.models import TimeStampedModel, OrderedModel
from django.contrib.gis.db.models import signals as signals
from django.dispatch.dispatcher import receiver

import geocoder

class EventWatcher(TimeStampedModel):
    event = models.CharField(_('Событие'), max_length=255, db_index=True)
    element = models.CharField(_('Элемент'), max_length=100, blank=True)
    page = models.CharField(_('Страница'), max_length=255, blank=True, db_index=True)
    ip = models.GenericIPAddressField(_('IP адрес'), blank=True, null=True)
    user = models.ForeignKey(LizartUser, on_delete=models.SET_NULL, null=True)
    lat = models.FloatField(_('Широта'), blank=True, null=True, db_index=True)
    lng = models.FloatField(_('Долгота'), blank=True, null=True, db_index=True)
    location = models.CharField(_('Местоположение'), max_length=100, blank=True)

    def __str__(self):
        return '%s: %s' % (self.page, self.event)

def add_location(sender, instance, created, *args, **kwargs):
    if created and instance.lat:
        try:
            g = geocoder.yandex(
                [instance.lat, instance.lng],
                method='reverse', lang='ru')
            city = g.city
            instance.location = city
            instance.save(created=False)
        except:
            pass

signals.post_save.connect(add_location, sender=EventWatcher)
