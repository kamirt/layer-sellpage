import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = ''

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [
    'constance.backends.database',
    'constance',
    'grappelli',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'autoslug',
    'rest_framework',
    'import_export',
    'corsheaders',
    # 'compressor',
    'ckeditor',
    # apps
    'core',
    'core.stats',
    'lizart_profile',
    'articles'
]

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'Advanced',
    },
}
CKEDITOR_UPLOAD_PATH = 'content/ckeditor/'
# Constance config

CONSTANCE_CONFIG = {
    'MAIN_DESCRIPTION':
        ('Описание', 'Промо-описание для продвижения главной страницы', str),
    'MAIN_BLOCKQUOTE':
        ('У вас есть только одна попытка правильно вести себя в суде', 'цитата на главную', str)
}
CONSTANCE_CONFIG_FIELDSETS = {
    'Настройки': ('MAIN_DESCRIPTION', 'MAIN_BLOCKQUOTE'),
}
CONSTANCE_BACKEND = 'constance.backends.database.DatabaseBackend'

AUTOSLUG_SLUGIFY_FUNCTION = 'core.utils.slugify.slugify'

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

CORS_ORIGIN_ALLOW_ALL = True
# CORS_ORIGIN_WHITELIST = (
#     '127.0.0.1:5006',
# )
ROOT_URLCONF = 'core.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'core.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# grappelli

GRAPPELLI_ADMIN_TITLE='Юридическое агентство Олега Крупкина'

# Internationalization


LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True
TELEGRAM_TOKEN = '541536757:AAFBWTWSLjt9cRp-thX1wGfUZAxC2CUoeFk'
# Static & media
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(os.path.dirname(__file__), 'static')

MEDIA_ROOT = os.path.join(os.path.dirname(__file__), 'media')

MEDIA_URL = '/media/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.FileSystemFinder',
    # 'compressor.finders.CompressorFinder',
    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Auth

AUTH_USER_MODEL = 'lizart_profile.LizartUser'

try:
    from core.settings_local import *
except:
    raise Exception('Local settings file does not exists!')
