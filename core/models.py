from django.db import models

# Abstract classes

class TimeStampedModel(models.Model):
    date_added = models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Дата добавления')
    date_updated = models.DateTimeField(auto_now=True, db_index=True, verbose_name='Дата обновления')

    class Meta(object):
        abstract = True

class OrderedModel(models.Model):
    order = models.PositiveIntegerField(default=0, verbose_name='Порядок')

    class Meta(object):
        abstract = True
        ordering = ['order']
