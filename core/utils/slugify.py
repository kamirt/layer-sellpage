from django import conf
from django.utils import encoding
from django.template import defaultfilters as src_pkg
from django.template.defaultfilters import slugify as dj_slugify
import unidecode

def slugify(value):
    value = encoding.smart_text(value)
    return dj_slugify(encoding.smart_text(unidecode.unidecode(value)))
