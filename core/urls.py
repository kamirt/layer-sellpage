# -*- coding: utf-8 -*-

from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin
from django.utils.encoding import iri_to_uri
from django.views.static import serve as staticserve
from django.urls import path

from rest_framework import routers, serializers, viewsets
from .views import home

urlpatterns = [
    path('', home),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    path('grappelli/', include('grappelli.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include('rest_framework.urls')),
    url(r'^api/articles/', include('articles.urls')),
    url(r'^api/events/', include('core.stats.urls'))
]

if settings.DEBUG:
    media_url = settings.MEDIA_URL[1:] if settings.MEDIA_URL.startswith('/') \
        else settings.MEDIA_URL

    urlpatterns += (
        url(r'^{0}(?P<path>.*)$'.format(iri_to_uri(media_url)),
         staticserve, {'document_root': settings.MEDIA_ROOT}
         ),
    )
