from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser
from django.contrib.gis.db.models import signals as signals
from django.dispatch.dispatcher import receiver

class LizartUser(AbstractUser):
    bio = models.TextField(_('История'), max_length=500, blank=True)
    location = models.CharField(_('Местоположение'), max_length=100, blank=True)
    birth_date = models.DateField(_('Дата рождения'), null=True, blank=True)
    ip_address = models.GenericIPAddressField(_('IP адрес'), blank=True, null=True)
    def __str__(self):
        return self.first_name + '_' + self.last_name



def create_admin_user(app_config, **kwargs):
    if app_config.name != 'lizart_profile':
        return None

    try:
        LizartUser.objects.get(username='admin')
    except LizartUser.DoesNotExist:
        print('Creating admin user: login: admin, password: 123')
        assert LizartUser.objects.create_superuser('admin', 'admin@localhost', '123')
    else:
        print('Admin user already exists')


signals.post_migrate.connect(create_admin_user)
