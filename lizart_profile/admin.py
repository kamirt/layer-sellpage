from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django import forms
from .models import LizartUser
# Register your models here.
class LizartUserAdmin(UserAdmin):
    list_display = ['email', 'first_name', 'is_staff']
    # fieldsets = UserAdmin.fieldsets + (
    #     (None,
    #         {'fields': ('email', 'password',)}),
    # )
admin.site.unregister(Group)
admin.site.register(LizartUser, LizartUserAdmin)
