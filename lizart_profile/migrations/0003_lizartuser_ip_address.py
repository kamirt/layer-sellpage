# Generated by Django 2.0.2 on 2018-03-03 14:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lizart_profile', '0002_auto_20180215_1809'),
    ]

    operations = [
        migrations.AddField(
            model_name='lizartuser',
            name='ip_address',
            field=models.GenericIPAddressField(blank=True, null=True, verbose_name='IP адрес'),
        ),
    ]
