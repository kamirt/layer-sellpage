from django.apps import AppConfig

class LizartProfileConfig(AppConfig):
    name = 'lizart_profile'
    verbose_name='Профиль'
    label = 'profile'
